import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:personalexpensesappflutter/models/day_spending.dart';

class ChartBar extends StatelessWidget {
  final DaySpending daySpending;
  final double totalAmount;

  ChartBar(this.daySpending, this.totalAmount);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      return Container(
          height: constraints.maxHeight,
          child: Column(
            children: <Widget>[
              Container(
                height: constraints.maxHeight * 0.15,
//                child: FittedBox(
                  child: Text(
                      "${NumberFormat.compactCurrency(symbol: "\$", decimalDigits: 1).format(daySpending.amount)}",
                      style: TextStyle(
                        fontSize: 10,
                        fontFamily: 'Orbitron',
                      )),
//                ),
              ),
              SizedBox(
                height: constraints.maxHeight * 0.05,
              ),
              Container(
                width: 10,
                height: constraints.maxHeight * 0.6,
                child: Stack(
                  alignment: Alignment.bottomCenter,
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                          color: Colors.grey,
                          border:
                              Border.all(color: Colors.blueGrey, width: 1.0),
                          borderRadius: BorderRadius.circular(10)),
                    ),
                    FractionallySizedBox(
                      heightFactor: daySpending.amount == 0
                          ? 0.0
                          : daySpending.amount / totalAmount,
                      child: Container(
                        decoration: BoxDecoration(
                          color: Theme.of(context).accentColor,
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(
                height: constraints.maxHeight * 0.05,
              ),
              Container(
                height: constraints.maxHeight * 0.15,
                child: FittedBox(
                    child: Text(
                        "${DateFormat.E().format(DateTime.fromMillisecondsSinceEpoch(daySpending.dayTimestampSeconds * 1000))}")),
              ),
            ],
          ));
    });
  }
}
