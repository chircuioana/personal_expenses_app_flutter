import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../../models/transaction.dart';

class NewTransaction extends StatefulWidget {
  final Function onAddNewTransaction;

  NewTransaction(this.onAddNewTransaction);

  @override
  State<StatefulWidget> createState() {
    return _NewTransactionState();
  }
}

enum _TransactionError { FIELD_REQUIRED, INVALID_AMOUNT }

class _NewTransactionState extends State<NewTransaction> {
  final nameController = TextEditingController();
  final amountController = TextEditingController();

  DateTime transactionDate;
  List<bool> _typeSelections = [true, false];

  _TransactionError _nameError;
  _TransactionError _amountError;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        width: double.infinity,
        padding: EdgeInsets.only(
            left: 10,
            top: 10,
            right: 10,
            bottom: MediaQuery.of(context).viewInsets.bottom + 10),
        margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
        child: Container(
          padding: EdgeInsets.all(5),
          child: Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.symmetric(vertical: 3, horizontal: 10),
                child: TextField(
                  decoration: InputDecoration(
                      icon: Icon(Icons.assignment),
                      hintText: "Name",
                      hasFloatingPlaceholder: true,
                      errorText: _getNameError(),
                      labelStyle: TextStyle(color: Colors.black45),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          borderSide: BorderSide(
                              color: Theme.of(context).accentColor))),
                  controller: nameController,
                  cursorColor: Theme.of(context).accentColor,
                  keyboardType: TextInputType.text,
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 3, horizontal: 10),
                child: TextField(
                  decoration: InputDecoration(
                      icon: Icon(Icons.attach_money),
                      hintText: "Amount",
                      hasFloatingPlaceholder: true,
                      errorText: _getAmountError(),
                      labelStyle: TextStyle(color: Colors.black45),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          borderSide: BorderSide(
                              color: Theme.of(context).accentColor))),
                  controller: amountController,
                  cursorColor: Theme.of(context).accentColor,
                  keyboardType: TextInputType.number,
                ),
              ),
              Container(
                alignment: Alignment.centerRight,
                margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Text(
                      transactionDate == null
                          ? "Choose date"
                          : DateFormat("dd MMMM yyyy").format(transactionDate),
                      style: transactionDate == null
                          ? TextStyle(
                              color: Colors.grey,
                              fontSize: 14,
                              fontStyle: FontStyle.italic,
                              fontWeight: FontWeight.w500)
                          : TextStyle(
                              color: Colors.black87,
                              fontSize: 14,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w700),
                    ),
                    IconButton(
                      icon: Icon(Icons.calendar_today),
                      onPressed: () => _showCalendarPicker(),
                    )
                  ],
                ),
              ),
              Container(
                alignment: Alignment.centerRight,
                padding: EdgeInsets.symmetric(vertical: 3, horizontal: 10),
                child: ToggleButtons(
                  children: <Widget>[
                    Container(
                      alignment: Alignment.center,
                      width: 100,
                      padding:
                          EdgeInsets.symmetric(vertical: 0, horizontal: 15),
                      child: Text(
                        "Expense",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      width: 100,
                      padding:
                          EdgeInsets.symmetric(vertical: 0, horizontal: 15),
                      child: Text(
                        "Income",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ],
                  isSelected: _typeSelections,
                  borderRadius: BorderRadius.circular(5),
                  selectedColor: Theme.of(context).accentColor,
                  color: Colors.black45,
                  onPressed: (int index) {
                    setState(() {
                      _typeSelections = List.generate(2, (listIndex) {
                        if (index == listIndex) {
                          return true;
                        }
                        return false;
                      });
                    });
                  },
                ),
              ),
              Container(
                  alignment: Alignment.centerRight,
                  child: FlatButton(
                    onPressed: _attemptAddNewTransaction,
                    child: Text("Add transaction"),
                    textColor: Theme.of(context).accentColor,
                  )),
            ],
          ),
        ),
      ),
    );
  }

  void _showCalendarPicker() {
    showDatePicker(
            context: context,
            initialDate:
                transactionDate == null ? DateTime.now() : transactionDate,
            firstDate: DateTime.now().subtract(Duration(days: 316)),
            lastDate: DateTime.now())
        .then((value) {
      transactionDate = value;
    });
  }

  void _attemptAddNewTransaction() {
    if (!_checkNameForErrors() ||
        !_checkAmountForError() ||
        !_checkForDateError()) {
      return;
    }
    double amount = double.parse(amountController.value.text);
    TransactionType type = _typeSelections[0] == true
        ? TransactionType.EXPENSE
        : TransactionType.INCOME;
    widget.onAddNewTransaction(Transaction(
        value: amount,
        type: type,
        name: nameController.text,
        timestampSeconds: transactionDate.millisecondsSinceEpoch ~/ 1000));
    _reset();
    Navigator.of(context).pop();
  }

  bool _checkNameForErrors() {
    if (nameController.text.isEmpty) {
      setState(() {
        _nameError = _TransactionError.FIELD_REQUIRED;
      });
      return false;
    }
    _nameError = null;
    return true;
  }

  bool _checkAmountForError() {
    if (amountController.text.isEmpty) {
      setState(() {
        _amountError = _TransactionError.FIELD_REQUIRED;
      });
      return false;
    }
    double amount = double.tryParse(amountController.value.text);
    if (amount == null) {
      setState(() {
        _amountError = _TransactionError.INVALID_AMOUNT;
      });
      return false;
    }
    _amountError = null;
    return true;
  }

  bool _checkForDateError() {
    if (transactionDate == null) return false;
    return true;
  }

  String _getNameError() {
    if (_nameError == _TransactionError.FIELD_REQUIRED) {
      return "This field is required";
    }
    return null;
  }

  String _getAmountError() {
    if (_amountError == _TransactionError.FIELD_REQUIRED) {
      return "This field is required";
    }
    if (_amountError == _TransactionError.INVALID_AMOUNT) {
      return "This is an invalid amount";
    }
    return null;
  }

  void _reset() {
    _typeSelections = [true, false];
    nameController.clear();
    amountController.clear();
  }
}
