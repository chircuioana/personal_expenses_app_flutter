import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../../models/transaction.dart';
import 'no_transactions.dart';

class TransactionList extends StatelessWidget {
  final List _transactions;
  final Function _onDeleteTransaction;

  TransactionList(this._transactions, this._onDeleteTransaction);

  @override
  Widget build(BuildContext context) {
    return _transactions.length == 0
        ? NoTransactions()
        : ListView.builder(
            itemCount: _transactions.length,
            itemBuilder: (BuildContext context, int index) =>
                _buildItemBody(context, _transactions[index]),
          );
  }

  Widget _buildItemBody(BuildContext context, Transaction transaction) {
    return ListTile(
      leading: FittedBox(
        fit: BoxFit.fitWidth,
        child: Container(
          width: 75,
          margin: EdgeInsets.symmetric(vertical: 7, horizontal: 7),
          child: Text(
            "${NumberFormat.compactCurrency(symbol: "\$", decimalDigits: 2).format(transaction.value)}",
            style: TextStyle(
                fontSize: 14,
                fontFamily: 'Orbitron',
                color: transaction.type == TransactionType.EXPENSE
                    ? Colors.red
                    : Colors.green),
            textAlign: TextAlign.center,
          ),
        ),
      ),
      title: Container(
        child: Text(
          "${transaction.name}",
          style: Theme.of(context).textTheme.title,
          textAlign: TextAlign.start,
        ),
      ),
      subtitle: Container(
          child: Text(
        "${DateFormat("dd MMM yyyy").format(DateTime.fromMillisecondsSinceEpoch(transaction.timestampSeconds * 1000))}",
        style: Theme.of(context).textTheme.subtitle,
        textAlign: TextAlign.start,
      )),
      trailing: Container(
          child: IconButton(
        icon: Icon(Icons.delete_outline),
        onPressed: () => _deleteTransaction(transaction),
      )),
    );
  }

  void _deleteTransaction(Transaction transaction) {
    _onDeleteTransaction(transaction);
  }
}
