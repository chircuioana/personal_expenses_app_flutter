import 'package:flutter/material.dart';
import 'package:personalexpensesappflutter/models/day_spending.dart';
import 'package:personalexpensesappflutter/models/transaction.dart';
import 'package:personalexpensesappflutter/utils/date_utils.dart';

import 'chart_bar.dart';

class Chart extends StatefulWidget {
  final List<Transaction> weekTransactions;

  Chart({this.weekTransactions});

  @override
  State createState() {
    return _ChartState();
  }
}

class _ChartState extends State<Chart> {
  List<DaySpending> _spendingList;
  double _totalAmount;

  @override
  Widget build(BuildContext context) {
    _spendingList = _generateWeekSpending();
    _totalAmount = _spendingList.map((spending) {
      return spending.amount;
    }).fold(0, (sum, amount) {
      return sum + amount;
    });

    return Container(
      padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
      child: Card(
          elevation: 10,
          child: Container(
            padding: EdgeInsets.all(5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                ..._spendingList.reversed.map((daySpending) {
                  return ChartBar(daySpending, _totalAmount);
                }).toList()
              ],
            ),
          )),
    );
  }

  List<DaySpending> _generateWeekSpending() {
    var spending = List<DaySpending>(7);
    for (var i = 0; i < 7; i++) {
      int day =
          DateTime.now().subtract(Duration(days: i)).millisecondsSinceEpoch ~/
              1000;
      var dayTransactions = widget.weekTransactions.where((transaction) {
        return DateUtils.isSameDay(day, transaction.timestampSeconds);
      }).toList();
      double daySpending = dayTransactions.fold(0, (sum, transaction) {
        double amount =
            transaction.type == TransactionType.EXPENSE ? transaction.value : 0;
        return sum + amount;
      });
      spending[i] = DaySpending(amount: daySpending, dayTimestampSeconds: day);
    }
    return spending;
  }
}
