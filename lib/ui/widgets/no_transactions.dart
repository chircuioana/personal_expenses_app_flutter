import 'package:flutter/material.dart';

class NoTransactions extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Image.asset('assets/images/no_transactions.png'),
        Text(
          "There are no transactions added",
          style: Theme.of(context).textTheme.title,
        ),
        Text(
          "Try adding some by taping the plus buttons.",
          style: Theme.of(context).textTheme.subtitle,
        ),
      ],
    );
  }
}
