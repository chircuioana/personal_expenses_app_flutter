import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:intl/intl.dart';
import 'package:personalexpensesappflutter/models/transaction.dart';
import 'package:personalexpensesappflutter/ui/widgets/new_transaction.dart';
import '../store/transactions_store.dart';
import '../ui/widgets/transactions_list.dart';
import 'widgets/chart.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return
//      Platform.isIOS
//        ? CupertinoApp(
//            title: 'Personal Expenses',
//            theme: CupertinoThemeData(
//              primaryColor: Colors.blueGrey,
//              primaryContrastingColor: Colors.white,
//              scaffoldBackgroundColor: Colors.white,
//              barBackgroundColor: Colors.blueGrey,
//            ),
//            home: MyHomePage(title: 'Personal Expenses'),
//          )
//        :
        MaterialApp(
      title: 'Personal Expenses',
      theme: ThemeData(
          primarySwatch: Colors.blueGrey,
          accentColor: Colors.deepOrange,
          fontFamily: "Lato",
          textTheme: ThemeData.light().textTheme.copyWith(
              title: TextStyle(
                color: Colors.black87,
                fontSize: 20,
                fontWeight: FontWeight.w700,
              ),
              subtitle: TextStyle(
                color: Colors.black38,
                fontSize: 12,
              )),
          appBarTheme: AppBarTheme(
            elevation: 10,
            textTheme: ThemeData.light().textTheme.copyWith(
                    title: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w700,
                )),
          )),
      home: MyHomePage(title: 'Personal Expenses'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  final String title;

  MyHomePage({Key key, this.title}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final store = TransactionsStore();
  bool isChartView = true;

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);

    final PreferredSizeWidget appBar = Platform.isIOS
        ? CupertinoNavigationBar(
            backgroundColor: Theme.of(context).primaryColor,
            actionsForegroundColor: Colors.white,
            middle: Text(
              widget.title,
              style: TextStyle(color: Colors.white),
            ),
            trailing: Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                if (mediaQuery.orientation == Orientation.landscape)
                  GestureDetector(
                    onTap: () => _toggleViewType(),
                    child: Icon(isChartView
                        ? CupertinoIcons.collections
                        : CupertinoIcons.book),
                  ),
                GestureDetector(
                  child: Icon(Icons.add),
                  onTap: () => _startAddNewTransaction(context),
                )
              ],
            ),
          )
        : AppBar(
            title: Text(widget.title),
            actions: <Widget>[
              if (mediaQuery.orientation == Orientation.landscape)
                IconButton(
                  icon: Icon(isChartView ? Icons.list : Icons.insert_chart),
                  onPressed: () => _toggleViewType(),
                ),
              IconButton(
                icon: Icon(Icons.add),
                onPressed: () => _startAddNewTransaction(context),
              )
            ],
          );

    final contentHeight = mediaQuery.size.height -
        appBar.preferredSize.height -
        mediaQuery.padding.top;

    return Platform.isIOS
        ? CupertinoPageScaffold(
            navigationBar: appBar,
            child: SafeArea(
              child: Scaffold(
                body: _getContent(mediaQuery, contentHeight),
              ),
            ),
          )
        : Scaffold(
            appBar: appBar,
            body: _getContent(mediaQuery, contentHeight),
            floatingActionButton: FloatingActionButton(
              child: Icon(Icons.add),
              onPressed: () => _startAddNewTransaction(context),
            ),
            floatingActionButtonLocation:
                FloatingActionButtonLocation.centerFloat,
          );
  }

  void _toggleViewType() {
    setState(() {
      isChartView = !isChartView;
    });
  }

  Widget _getContent(MediaQueryData mediaQuery, double contentHeight) {
    return Container(
        width: double.infinity,
        height: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            if (mediaQuery.orientation == Orientation.landscape)
              ..._getLayoutLandscape(contentHeight),
            if (mediaQuery.orientation == Orientation.portrait)
              ..._getLayoutPortrait(contentHeight),
          ],
        ));
  }

  List<Widget> _getLayoutPortrait(double contentHeight) {
    return [
      _getChart(contentHeight * 0.3),
      _getList(contentHeight * 0.7),
    ].toList();
  }

  List<Widget> _getLayoutLandscape(double contentHeight) {
    return [
      if (isChartView) _getChart(contentHeight),
      if (!isChartView) _getList(contentHeight),
    ].toList();
  }

  Widget _getChart(double height) {
    return Container(
      padding: EdgeInsets.only(left: 0, top: 0, right: 0, bottom: 30),
      height: height,
      child: Chart(
        weekTransactions: store.getLastWeekTransactions(),
      ),
    );
  }

  Widget _getList(double height) {
    return Container(
      height: height,
      child: TransactionList(store.transactions, (transaction) {
        _deleteTransaction(transaction);
      }),
    );
  }

  void _startAddNewTransaction(BuildContext context) {
    showModalBottomSheet(
        context: context,
        builder: (_) {
          return NewTransaction((newTransaction) {
            _addNewTransaction(newTransaction);
          });
        });
  }

  void _addNewTransaction(newTransaction) {
    setState(() {
      store.addTransaction(newTransaction);
    });
  }

  void _deleteTransaction(transaction) {
    setState(() {
      store.deleteTransaction(transaction);
    });
  }
}
