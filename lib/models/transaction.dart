enum TransactionType { EXPENSE, INCOME }

class Transaction {
  final TransactionType type;
  final double value;
  final String name;
  final int timestampSeconds;

  const Transaction({this.value, this.type, this.name, this.timestampSeconds});
}
