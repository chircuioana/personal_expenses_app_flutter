class DaySpending {
  final double amount;
  final int dayTimestampSeconds;

  DaySpending({this.amount, this.dayTimestampSeconds});
}
