import 'package:personalexpensesappflutter/models/transaction.dart';

class TransactionsStore {
  var transactions = List<Transaction>();

  void addTransaction(Transaction statement) {
    transactions.add(statement);
    transactions
        .sort((a, b) => a.timestampSeconds.compareTo(b.timestampSeconds));
  }

  void deleteTransaction(Transaction transaction) {
    transactions.removeWhere((item) {
      return item.hashCode == transaction.hashCode;
    });
  }

  List<Transaction> getLastWeekTransactions() {
    return transactions.where((transaction) {
      var today = DateTime.now();
      var transactionDay = DateTime.fromMillisecondsSinceEpoch(
          transaction.timestampSeconds * 1000);
      return transactionDay.isAfter(today.subtract(Duration(days: 7)));
    }).toList();
  }
}
