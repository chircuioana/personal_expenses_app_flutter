import 'package:intl/intl.dart';

class DateUtils {
  static bool isSameDay(day1TimestampSeconds, day2TimestampSeconds) {
    final DateTime day1 =
        DateTime.fromMillisecondsSinceEpoch(day1TimestampSeconds * 1000);
    final DateTime day2 =
        DateTime.fromMillisecondsSinceEpoch(day2TimestampSeconds * 1000);

    return day1.day == day2.day &&
        day1.month == day2.month &&
        day1.year == day2.year;
  }
}
